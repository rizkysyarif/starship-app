import { createRouter, createWebHistory } from "vue-router";
import Starship from '@/pages/Starship/index.vue';
import StarshipDetail from '@/pages/StarshipDetail/index.vue';

const routes = [
    {
        path: '/',
        name: 'Starship',
        component: Starship,
    },
    {
        path: '/details',
        name: 'Details',
        component: StarshipDetail,
        props: true
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router;