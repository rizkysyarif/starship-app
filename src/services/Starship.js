import http from "../http-common";

class StarshipService {
    getAll() {
        return http.get("/starships");
    }
    findByKeyword(keyword) {
        return http.get(`/starships?search=${keyword}`);
    }
    findById(id) {
        return http.get(`/starships/${id}`);
    }
}

export default new StarshipService();