# Starship App

> Simple List Starship
 
## Table of contents

> * [Starship App](#starship-app)
>   * [Table of contents](#table-of-contents)
>   * [Introduction](#introduction)
>   * [Deployment](#deployment)
>   * [Features](#features)
>   * [Installation](#installation)
>     * [Clone](#clone)
>     * [Install Depedencies](#install-depedencies)
>     * [Run server development](#run-server-development)
>   * [Dependencies](#dependencies)
>   * [License](#license)

## Introduction

> This is simple List Starship using VueJs with infinite scroll, search and responsive ui.

## Deployment

> Live preview in https://startship-app.netlify.app/

## Features
  - Infinite Scroll
  - Search Product
  - Detail Product
 
## Installation

### Clone
```
$ git clone https://gitlab.com/rizkysyarif/starship-app.git
$ cd starship-app
```

### Install Depedencies
```
npm install
```

### Run server development
```
npm start
```

## Dependencies
```
- Axios
- Vue
- Vue Router
- Vue Use
- Bootstrap
- Eslint
```

## License
MIT


 
 